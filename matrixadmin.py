# Copyright (C) 2022 AgoraSMP Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from enum import Enum
import requests
from urllib import parse
import json

from django.conf import settings


class Presence(Enum):
    ONLINE = "online"
    OFFLINE = "offline"
    UNAVAILABLE = "unavailable"


class MatrixAdmin:
    def __init__(self):
        self.base = settings.MATRIX_BASE_DOMAIN
        self.host = settings.MATRIX_HOST
        self.identity = settings.MATRIX_IDENTITY
        self.access_token = settings.MATRIX_ACCESS_TOKEN

    def apicall(self, path, admin=False, method="GET", data="{}", version=None):
        if version is None:
            version = 2 if admin else 3
        assert isinstance(version, int), "version must be integer"
        base_path = "/_synapse/admin/v" if admin else "/_matrix/client/v"
        if not isinstance(data, str):
            data = json.dumps(data)
        fullpath = self.host.rstrip("/") + base_path + str(version) + "/" + path
        headers = {"Authorization": "Bearer " + self.access_token}
        if method == "GET":
            r = requests.get(fullpath, headers=headers)
        elif method == "POST":
            r = requests.post(fullpath, headers=headers, data=data)
        elif method == "PUT":
            r = requests.put(fullpath, headers=headers, data=data)
        elif method == "DELETE":
            r = requests.delete(fullpath, headers=headers, data=data)
        else:
            raise Exception("unknown method {}".format(method))
        return r.json()

    def matrix_id(self, username):
        return f"@{username}:{self.base}"

    def matrix_alias(self, name):
        return f"#{name}:{self.base}"

    def user_exists(self, username):
        print("admin user_exists")
        j = self.user_details(username)
        errcode = j.get("errcode", None)
        if errcode is not None:
            if errcode == "M_NOT_FOUND":
                return False
            raise Exception("Query failed: {}".format(j["error"]))
        return True

    def user_details(self, username):
        matrix_id = self.matrix_id(username)
        return self.apicall("users/{}".format(matrix_id), admin=True)

    def user_create_or_modify(self, username, displayname=None, email=None):
        matrix_id = self.matrix_id(username)
        j = dict()
        if displayname:
            j["displayname"] = displayname
        if email:
            j["threepids"] = [{"medium": "email", "address": email}]
        return self.apicall(
            "users/{}".format(matrix_id), admin=True, method="PUT", data=j
        )

    def user_deactivate(self, username, erase=False):
        matrix_id = self.matrix_id(username)
        j = {"erase": erase}
        return self.apicall(
            "deactivate/{}".format(matrix_id),
            admin=True,
            method="POST",
            data=j,
            version=1,
        )

    def room_create(self, name, alias=None, invite=None):
        j = {"name": name}
        if alias:
            j["room_alias_name"] = alias
        # either way, the bot will always become a member of the room implicitly (as its creator)
        if invite:
            j["invite"] = list(map(self.matrix_id, invite))
        return self.apicall("createRoom", method="POST", data=j)

    def room_delete(self, room_id, purge=True, block=False):
        j = {
            "purge": purge,
            "block": block,
        }
        return self.apicall(
            "rooms/{}".format(room_id), admin=True, method="DELETE", data=j
        )

    def room_invite_user(self, room_id, username, reason=None):
        matrix_id = self.matrix_id(username)
        j = {"user_id": matrix_id}
        if reason:
            j["reason"] = reason
        return self.apicall("rooms/{}/invite".format(room_id), method="POST", data=j)

    def room_kick_user(self, room_id, username, reason=None):
        matrix_id = self.matrix_id(username)
        j = {"user_id": matrix_id}
        if reason:
            j["reason"] = reason
        return self.apicall("rooms/{}/kick".format(room_id), method="POST", data=j)

    def room_alias_to_id(self, alias):
        # careful, there's a literal '#' in here, which we need
        # to URL encode first
        alias = parse.quote(self.matrix_alias(alias))
        resp = self.apicall("directory/room/{}".format(alias))
        if "errcode" in resp:
            if resp["errcode"] == "M_NOT_FOUND":
                return None
            raise Exception(resp["error"])
        return resp["room_id"]

    def room_alias_exists(self, alias):
        return self.room_alias_to_id(alias) is not None

    # server notices must be enabled in synapse for this to work
    def server_notice(self, text):
        j = {"user_id": self.identity, "content": {"msgtype": "m.text", "body": text}}
        return self.apicall("send_server_notice", method="POST", data=j, version=1)

    def set_presence(self, presence, message=None):
        j = {"presence": presence.value}
        if message:
            j["status_msg"] = message
        return self.apicall(
            "presence/{}/status".format(self.identity), method="PUT", data=j
        )
