# Copyright (C) 2022 AgoraSMP Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from celery import shared_task
from celery.exceptions import Retry
from celery.utils.log import get_task_logger
from django.contrib.auth.models import User

logger = get_task_logger(__name__)

from usermgr.models import Seminar, Workgroup, Profile, Event

from agora_synapse.matrixadmin import MatrixAdmin, Presence

## HELPERS


def get_matrix_room_alias(context):
    if isinstance(context, Seminar):
        return f"s-{context.slug}"
    elif isinstance(context, Workgroup):
        return f"w-{context.slug}"
    else:
        raise ValueError(
            "Invalid type, must be Seminar or Workgroup, not " + str(type(context))
        )


def invite_to_room(t, username, alias):
    client = MatrixAdmin()
    room_id = client.room_alias_to_id(alias)
    if room_id is None:
        logger.error(
            "Tried to invite %s to Matrix room %s, but room does not exist (yet?). Will retry later.",
            username,
            alias,
        )
        raise t.retry()
    client.room_invite_user(room_id, username)


def kick_from_room(username, alias):
    client = MatrixAdmin()
    room_id = client.room_alias_to_id(alias)
    if room_id is None:
        logger.error(
            "Tried to kick %s from Matrix room %s, but room does not exist (anymore?).",
            username,
            alias,
        )
    client.room_kick_user(room_id, username)


## EVENT HANDLERS


@shared_task(bind=True, retry_backoff=True, acks_late=True)
def handle_user_registered(self, profile):
    try:
        profile = Profile.objects.get(pk=profile)
    except Profile.DoesNotExist:
        logger.error(
            "Tried to fetch profile with id %s, but Profile object does not exist (anymore?)",
            profile,
        )
        return

    user = profile.user
    username = user.username
    displayname = f"{user.first_name} {user.last_name}"
    email = user.email

    client = MatrixAdmin()
    if not client.user_exists(username):
        client.user_create_or_modify(username, displayname=displayname, email=email)
    else:
        logger.warn(
            "Tried to create Matrix user %s, but account already existed", username
        )


@shared_task(bind=True, retry_backoff=True, acks_late=True)
def handle_user_name_changed(self, user):
    try:
        user = User.objects.get(pk=user)
    except User.DoesNotExist:
        logger.error(
            "Tried to fetch user with id %s, but User object does not exist (anymore?)",
            user,
        )
        return

    username = user.username
    displayname = f"{user.first_name} {user.last_name}"

    client = MatrixAdmin()
    if client.user_exists(username):
        client.user_create_or_modify(username, displayname=displayname)
    else:
        logger.warn(
            "Tried to rename Matrix user %s, but no such account existed", username
        )
    pass


@shared_task(bind=True, retry_backoff=True, acks_late=True)
def handle_user_email_changed(self, user):
    try:
        user = User.objects.get(pk=user)
    except User.DoesNotExist:
        logger.error(
            "Tried to fetch user with id %s, but User object does not exist (anymore?)",
            user,
        )
        return

    username = user.username
    email = user.email

    client = MatrixAdmin()
    if client.user_exists(username):
        # TODO check if this properly removes the old email, or just adds another one
        client.user_create_or_modify(username, email=email)
    else:
        logger.warn(
            "Tried to change email for Matrix user %s, but no such account existed",
            username,
        )
    pass


@shared_task(bind=True, retry_backoff=True, acks_late=True)
def handle_user_workgroup_assigned__nonsynthetic(self, profile, workgroup):
    try:
        profile = Profile.objects.get(pk=profile)
    except Profile.DoesNotExist:
        logger.error(
            "Tried to fetch profile with id %s, but Profile object does not exist (anymore?)",
            profile,
        )
        return
    try:
        workgroup = Workgroup.objects.get(pk=workgroup)
    except Workgroup.DoesNotExist:
        logger.error(
            "Tried to fetch workgroup with id %s, but Workgroup object does not exist (anymore?)",
            workgroup,
        )
        return

    username = profile.user.username
    alias = get_matrix_room_alias(workgroup)

    invite_to_room(self, username, alias)


@shared_task(bind=True, retry_backoff=True, acks_late=True)
def handle_user_seminar_assigned__nonsynthetic(self, profile, seminar):
    try:
        profile = Profile.objects.get(pk=profile)
    except Profile.DoesNotExist:
        logger.error(
            "Tried to fetch profile with id %s, but Profile object does not exist (anymore?)",
            profile,
        )
        return
    try:
        seminar = Seminar.objects.get(pk=seminar)
    except Seminar.DoesNotExist:
        logger.error(
            "Tried to fetch seminar with id %s, but Seminar object does not exist (anymore?)",
            seminar,
        )
        return

    username = profile.user.username
    alias = get_matrix_room_alias(seminar)

    invite_to_room(self, username, alias)


@shared_task(bind=True, retry_backoff=True, acks_late=True)
def handle_user_workgroup_unassigned__nonsynthetic(self, profile, workgroup):
    try:
        profile = Profile.objects.get(pk=profile)
    except Profile.DoesNotExist:
        logger.error(
            "Tried to fetch profile with id %s, but Profile object does not exist (anymore?)",
            profile,
        )
        return
    try:
        workgroup = Workgroup.objects.get(pk=workgroup)
    except Workgroup.DoesNotExist:
        logger.error(
            "Tried to fetch workgroup with id %s, but Workgroup object does not exist (anymore?)",
            workgroup,
        )
        return

    username = profile.user.username
    alias = get_matrix_room_alias(workgroup)
    kick_from_room(username, alias)


@shared_task(bind=True, retry_backoff=True, acks_late=True)
def handle_user_seminar_unassigned__nonsynthetic(self, profile, seminar):
    try:
        profile = Profile.objects.get(pk=profile)
    except Profile.DoesNotExist:
        logger.error(
            "Tried to fetch profile with id %s, but Profile object does not exist (anymore?)",
            profile,
        )
        return
    try:
        seminar = Seminar.objects.get(pk=seminar)
    except Seminar.DoesNotExist:
        logger.error(
            "Tried to fetch seminar with id %s, but Seminar object does not exist (anymore?)",
            seminar,
        )
        return

    username = profile.user.username
    alias = get_matrix_room_alias(seminar)

    kick_from_room(username, alias)


@shared_task(bind=True, retry_backoff=True, acks_late=True)
def handle_user_gdpr_delete(self, username, userid):
    client = MatrixAdmin()
    if client.user_exists(username):
        client.user_deactivate(username, erase=True)
    else:
        logger.warn(
            "Tried to deactivate Matrix user %s, but no such account existed", username
        )


@shared_task(bind=True, retry_backoff=True, acks_late=True)
def handle_seminar_access_opens(self, seminar):
    try:
        seminar = Seminar.objects.get(pk=seminar)
    except Seminar.DoesNotExist:
        logger.error(
            "Tried to open access to seminar with id %s, but Seminar object does not exist (anymore?)",
            seminar,
        )
        return

    alias = get_matrix_room_alias(seminar)
    affected_profiles = Profile.objects.filter(
        id__in=seminar.workgroup_set.values_list("profile", flat=True).distinct()
    ).values_list("user__username", flat=True)

    client = MatrixAdmin()
    if client.room_alias_exists(alias):
        logger.warn(
            "Tried to create Matrix room #%s for seminar %s, but room alias was already taken",
            alias,
            seminar.slug,
        )
        # TODO check current membership state and update room membership to contain exactly affected_profiles
    else:
        client.room_create(seminar.name, alias=alias, invite=affected_profiles)


@shared_task(bind=True, retry_backoff=True, acks_late=True)
def handle_seminar_ends(self, seminar):
    logger.warning(
        "Sending messages to Matrix rooms is not supported yet by MatrixAdmin"
    )


@shared_task(bind=True, retry_backoff=True, acks_late=True)
def handle_seminar_access_closes(self, seminar):
    try:
        seminar = Seminar.objects.get(pk=seminar)
    except Seminar.DoesNotExist:
        logger.error(
            "Tried to open access to seminar with id %s, but Seminar object does not exist (anymore?)",
            seminar,
        )
        return

    alias = get_matrix_room_alias(seminar)

    client = MatrixAdmin()
    room_id = client.room_alias_to_id(alias)
    if room_id is None:
        logger.error(
            "Tried to purge Matrix room %s, but room does not exist (anymore?).", alias
        )
        return
    client.room_delete(room_id, purge=True, block=False)


@shared_task(bind=True, retry_backoff=True, acks_late=True)
def handle_workgroup_access_opens(self, workgroup, synthetic=None):
    try:
        workgroup = Workgroup.objects.get(pk=workgroup)
    except Workgroup.DoesNotExist:
        logger.error(
            "Tried to open access to workgroup with id %s, but Workgroup object does not exist (anymore?)",
            workgroup,
        )
        return

    alias = get_matrix_room_alias(workgroup)
    affected_profiles = workgroup.profile_set.values_list("user__username", flat=True)

    client = MatrixAdmin()
    if client.room_alias_exists(alias):
        logger.warn(
            "Tried to create Matrix room #%s for workgroup %s, but room alias was already taken",
            alias,
            workgroup.slug,
        )
        # TODO check current membership state and update room membership to contain exactly affected_profiles
    else:
        client.room_create(workgroup.name, alias=alias, invite=affected_profiles)


@shared_task(bind=True, retry_backoff=True, acks_late=True)
def handle_workgroup_access_closes(self, workgroup, synthetic=None):
    try:
        workgroup = Workgroup.objects.get(pk=workgroup)
    except Workgroup.DoesNotExist:
        logger.error(
            "Tried to open access to workgroup with id %s, but Workgroup object does not exist (anymore?)",
            workgroup,
        )
        return

    alias = get_matrix_room_alias(workgroup)

    client = MatrixAdmin()
    room_id = client.room_alias_to_id(alias)
    if room_id is None:
        logger.error(
            "Tried to purge Matrix room %s, but room does not exist (anymore?).", alias
        )
        return
    client.room_delete(room_id, purge=True, block=False)


@shared_task(bind=True, retry_backoff=True, acks_late=True)
def handle_event_upcoming(self, event):
    logger.warning(
        "Sending messages to Matrix rooms is not supported yet by MatrixAdmin"
    )


# to be called using Celery Beat once in a minute
@shared_task
def update_presence():
    client = MatrixAdmin()
    client.set_presence(Presence.ONLINE)
